/* GRANT y REVOKE */

/* GRANT */
GRANT SELECT ON Tabla_1 TO Juan;

/* REVOKE */
REVOKE SELECT ON Tabla_1 TO Juan;

/* GRANT */
GRANT SELECT, INSERT, UPDATE, DELETE ON Tabla_1 TO Juan;



/* COMMIT, ROLLBACK y SAVEPOINT */

/* COMMIT */
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
COMMIT;


/* ROLLBACK */
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
ROLLBACK;

/* SAVEPOINT */
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
SAVEPOINT Punto_Control;
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
ROLLBACK TO SAVEPOINT Punto_Control;



/* DEFAULT, CHECK, UNIQUE y IDENTITY */

/* DEFAULT */
CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(50),
	Edad INT,
    Puesto VARCHAR(50),
    Salario INT DEFAULT 2000
);

/* CHECK */
CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(50),
	Edad INT,
    Puesto VARCHAR(50),
	Salario INT CHECK (Salario >= 1000 AND Salario <= 2000)
);


/* UNIQUE */
CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(50),
	Edad INT,
    Puesto VARCHAR(50),
	Salario INT UNIQUE
);


/* IDENTITY */
CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(50),
	Edad INT,
    Puesto VARCHAR(50),
	Salario INT UNIQUE
);


